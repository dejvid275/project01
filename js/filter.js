document.querySelector("#filter-marketing").addEventListener("click", showMarketing);
document.querySelector("#filter-programming").addEventListener("click", showProgramming);
document.querySelector("#filter-design").addEventListener("click", showDesign);
showAll();


function showMarketing() {
    hideAll();
    document.querySelector("#marketing").checked = true;
    document.querySelector("#programming").checked = false;
    document.querySelector("#design").checked = false;

    if (document.querySelector("#marketing").checked) {
        document.querySelector("#filter-marketing").classList.add('checked', 'border-bottom', 'border-info', 'bg-danger');
        document.querySelector("#filter-programming").classList.remove('checked', 'border-bottom', 'border-info', 'bg-danger');
        document.querySelector("#filter-design").classList.remove('checked', 'border-bottom', 'border-info', 'bg-danger');

        var marketingCards = document.querySelectorAll(".marketing");
        for (var i = 0; i < marketingCards.length; i++) {
            marketingCards[i].style.display = "inline-block";
        }
    }
}

function showProgramming() {
    hideAll();
    document.querySelector("#programming").checked = true;
    document.querySelector("#marketing").checked = false;
    document.querySelector("#design").checked = false;

    if (document.querySelector("#programming").checked) {
        document.querySelector("#filter-programming").classList.add('checked', 'border-bottom', 'border-info', 'bg-danger');
        document.querySelector("#filter-marketing").classList.remove('checked', 'border-bottom', 'border-info', 'bg-danger');
        document.querySelector("#filter-design").classList.remove('checked', 'border-bottom', 'border-info', 'bg-danger');

        var programmingCards = document.querySelectorAll(".programming");
        for (var i = 0; i < programmingCards.length; i++) {
            programmingCards[i].style.display = "inline-block";
        }
    }
}

function showDesign() {
    hideAll();
    document.querySelector("#design").checked = true;
    document.querySelector("#programming").checked = false;
    document.querySelector("#marketing").checked = false;

    if (document.querySelector("#design").checked) {
        document.querySelector("#filter-design").classList.add('checked', 'border-bottom', 'border-info', 'bg-danger');
        document.querySelector("#filter-marketing").classList.remove('checked', 'border-bottom', 'border-info', 'bg-danger');
        document.querySelector("#filter-programming").classList.remove('checked', 'border-bottom', 'border-info', 'bg-danger');
        var designCards = document.querySelectorAll(".design");
        for (var i = 0; i < designCards.length; i++) {
            designCards[i].style.display = "inline-block";
        }
    
    } else {
        document.querySelector("#filter-design").classList.remove('checked');
        showAll();
    }        
}

function hideAll() {
    var feeds = document.querySelectorAll(".feed");
    for (var i = 0; i < feeds.length; i++) {
        feeds[i].style.display = "none";
    }
}

function showAll() {
        var feeds = document.querySelectorAll(".feed");
        for(var i = 0; i < feeds.length; i++) {
            feeds[i].style.display = "inline-block";
        }
    }